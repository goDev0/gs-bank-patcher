# GS Banks Patcher
This program allows to quickly patch the BANK binary files found in the leaks, which contain graphical assets like pokémon sprites. It only works with a Pokémon G/S ROM.

## Usage
```
gsbanks (original rom path) (banks folder)
``` 

### ROM
The rom is .gb, .gbc or similar file format.
ISX files must be converted to regular roms before applying banks with this program.

### BANKS
For a correct execution, the banks folder has to contain the following files:
* bank12.bin
* bank13.bin
* bank14.bin
* bank15.bin
* bank16.bin
* bank17.bin
* bank18.bin
* bank19.bin
* bank1a.bin
* bank1b.bin
* bank1c.bin
* bank1d.bin
* bank1e.bin
* bank1f.bin

These files are usually found in SOURCE/MONSDATA