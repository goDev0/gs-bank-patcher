#include <iostream>
#include <fstream>
#include <filesystem> //Compile with c++17
#include <string>
#include <map>
using namespace std;
namespace fs = std::filesystem;

#define BUFSIZE 256

/*
	FROM mons2/MAKE/README.txt
	bank12.bin -> 12:4000
	bank13.bin -> 1f:4000
	bank14.bin -> 20:4000
	bank15.bin -> 15:4000
	bank16.bin -> 16:4000
	bank17.bin -> 17:4000
	bank18.bin -> 18:4000
	bank19.bin -> 19:4000
	bank1a.bin -> 1a:4000
	bank1b.bin -> 1b:4000
	bank1c.bin -> 1c:4000
	bank1d.bin -> 1d:4000
	bank1e.bin -> 1e:4000
	bank1f.bin -> 2e:4000
*/
map<unsigned int, unsigned int> addresses{
		{0x12, 0x12},
		{0x13, 0x1f},
		{0x14, 0x20},
		{0x15, 0x15},
		{0x16, 0x16},
		{0x17, 0x17},
		{0x18, 0x18},
		{0x19, 0x19},
		{0x1a, 0x1a},
		{0x1b, 0x1b},
		{0x1c, 0x1c},
		{0x1d, 0x1d},
		{0x1e, 0x1e},
		{0x1f, 0x2e}
};

size_t move(ifstream &src, ofstream &dst)
{
	char buffer[BUFSIZE];

	size_t written = 0;
	size_t read;
	while (!src.eof())
	{
		src.read(buffer, BUFSIZE);
		read = src.gcount();
		dst.write(buffer, read);
		written += read;
	}

	return written;
}

size_t move(ifstream& src, fstream& dst)
{
	char buffer[BUFSIZE];

	size_t written = 0;
	size_t read;
	while (!src.eof())
	{
		src.read(buffer, BUFSIZE);
		read = src.gcount();
		dst.write(buffer, read);
		written += read;
	}

	return written;
}

int main(int argc, char* argv[])
{
	if (argc < 3)
	{
		cout << "Usage: " << fs::path(argv[0]).stem().string() << " (original rom path) (banks folder)" << endl;
		return 1;
	}

	fs::path romPath = argv[1];

	if (!fs::is_regular_file(romPath))
	{
		cout << romPath << " is not a file!" << endl;
		return 1;
	}

	fs::path banksFolder = argv[2];

	if (!fs::is_directory(banksFolder))
	{
		cout << romPath << " is not a directory!" << endl;
		return 1;
	}

	fs::path newRomName = romPath.stem().string()+"_patched"+romPath.extension().string();

	cout << "Rom size: " << fs::file_size(romPath) << endl;

	//Rom copy
	ifstream rom(romPath, ios::binary);
	ofstream newRom(newRomName, ios::binary);
	
	size_t written = move(rom, newRom);

	rom.close();
	newRom.close();

	cout << "Written size: " << written << endl;

	//Banks insertion
	fstream finalRom(newRomName, ios::binary | ios::in | ios::out);
	cout << endl;
	size_t bankNum;
	size_t address;
	for (auto &p : fs::directory_iterator(banksFolder))
	{
		if (fs::is_regular_file(p) && (p.path().extension() == ".BIN" || p.path().extension() == ".bin"))
		{
			cout << p.path() << endl;
			bankNum = stoul(p.path().stem().string().substr(p.path().stem().string().size() - 2, 2), nullptr, 16);
			address = addresses[bankNum];
			cout << "Bank "  << std::showbase << std::hex << bankNum << "->" << address << endl;

			ifstream bank(p.path(), ios::binary);

			address = address * 0x4000;
			finalRom.seekp(address);
			cout << "Writing at " << std::showbase << std::hex << finalRom.tellp() << endl;

			written = move(bank, finalRom);
			cout << "Written " << std::showbase << std::hex << written << " bytes" << endl;

			bank.close();

			cout << endl;
		}
	}
	finalRom.close();

	return 0;
}